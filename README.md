# czarny kruk

Robot, którego pierwszy projekt powstał w grudniu 2018 roku, w ramach przygotowania drużyny Team Rabyte
do startu w konkursie FRC. Budowa miała na celu rozwinąć podstawowe umiejętnosci z zakresu projektowania i modelowania w 3D, mechaniki oraz programowania.

### przyszłość

Robot w przyszlosci ma służyć jako narzędzie do testowania oraz rozwijania nowych mechanizmów transportu opartych o koła mecanum i sieć neuronową połączona z rozpoznawaniem obrazu. Aktualnie prowadzone są prace w tym kierunku, a już prototypowa wersja została zaimplementowana i umożliwia rysowanie i wysyłanie do robota przy użyciu kamerki biurowej kierunku w którum ma się poruszać. Następnym krokiem będzie detekcja obiektów i interakcja z nimi.

## elementy i podzespoły robota

### baza jezdna

Baza jezdna podobnie jak i cały szkielet robota został wykonany z anodowanych czarnych profili aluminiowych V-slot. Jest to rozwiązanie szczegonie wydajne w przypadku prototypów, gdyż umożliwia łatwe zmiany w konstrukcji, jednocześnie tworząc równe i trwałe krawędzie. Do bazy jezdnej przymocowane są za pomocą drukowanych w 3D uchwytów cztery silniki oraz opowiednio cztery koła mecanum.

### koła mecanum

Robot został wyposażony w cztery tego typu koła, co pozwala na poruszanie się w dowolnym możliwym kierunku, co nie jest możliwe w przypadku konwencjonalnych kół. Koła mecanum składają się ze swobodnie obracających się gumowych rolek przykręconych pod kątem do metalowej tarczy znajdującej się na obwodzie koła. Mecanum obracając się kreują wektor prędkości nachylony o dany kąt, równy kącie nachylenia rolek, w naszym przypadku jest to 45st. Dzięki odpowiedniemu ułożeniu kół wektory się odpoweidnio, znoszą lub wzmacjniają pozwalając na ruch w każdym kierunku.

### sterowniki i silniki

Do napędu robota zostąły użyte cztery jednokanałowe sterowniki Cytron MD13S oraz odpowiednio cztery silniki z przekładniami. Każdy ze sterowników sterowany jest za pomocą Arduino. Jako Input sterownik przyjmuje sygnal PWM (0-255) do 20kHz oraz sygnał informujący o kierunku poruszania silnikiem DIR (0 lub 1).

### zasilanie

Całość jest zasilana 12 bateriami litowo-jonowymi połączonymi szeregowo (3) i równolegle (4) dając w sumie 12V i 4000 mAh. Dodatkowo na bazie jezdnej znajduje się przetwornica Step-Down obniżająca napięcie do 5V i tym samym umożliwiająca działanie mikrokontrolerów.

### Arduino

Głownym zadaniem Arduino jest wysyłanie zadanej mocy na sterowniki. Arduino komunikuje się z RaspberryPi przy użyciu kabla USB typ B, działając w interfejsie USART. Komunikacja rozpoczyna się klauzulą "ready" wysyłaną z Arduino. Arduino dostaje w kolejności: numer silnika oraz wartość prędkości PWM jaką ma wysłać na silniki. numery silników przychodzą ponumerownae 1-4, natomiast prędkość przychodzi w zakresie od 100 do 610 i następnie jest mapowana na Arduino. Jeśli po mapowaniu wartość jest ujemna to następuje zmiana kierunku oraz wartość jest przemiaonowywana na dodatnią.

### router

Umożliwia użytkownikom w swojej sieci lokalnej na podgląd strony sterowania robotem.

### RaspberryPi 4b+ Wifi

Na tym małym komputerze został zainstalowany i skonfigutowany server Ubuntu 20.4.3 LTS oraz node 16.4 i kilka innych pakietow do obejrzenia w pliku package.json. To urządzenie odgrywa kluczową role jeślki chodzi o program. Na nim postawiony jest serwer przy użyciu node js i express. Serwer jest dostępny w sieci lokalnej pod adresem Raspberry po połączeniu się z routerem. Na stronie https serwera dostępne jest sterowanie, podgląd kamery wykrywania obrazu oraz podstawowe ustawienia.

##### Użyte biblioteki i skrypty na rpi:

###### ml5 - sztuczna inteligencja, detekcja obrazu
###### pl5.js - canvas i skryptu zwiazane z kamerą
###### npm serial-port - komunikacja z Arduino
###### npm socket.io - komunikacja serwera z uzytkownikiem (strona na ktorej jest)
###### npm express - obsługa serwer https
###### jQuerry - mechanika menu wyboru

### Web page (control-panel)

![This is an image](/bin/controll-menu.png)

### Robot ogólny diagram działania

![This is an image](/bin/black-raven-diagram.png)