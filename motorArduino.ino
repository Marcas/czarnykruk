int right_upper_motor = 5;
int left_upper_motor = 6;
int right_lower_motor = 10;
int left_lower_motor = 11;

int right_upper_motor_dir = 4;
int left_upper_motor_dir = 7;
int right_lower_motor_dir = 9;
int left_lower_motor_dir = 12;

char buf[4];

int motorNumber = 0;
int motorVelocity = 0;
int motorDirection = 0;

void setup() {
  pinMode(right_upper_motor, OUTPUT);
  pinMode(left_upper_motor, OUTPUT);
  pinMode(right_lower_motor, OUTPUT);
  pinMode(left_lower_motor, OUTPUT);

  pinMode(right_upper_motor_dir, OUTPUT);
  pinMode(left_upper_motor_dir, OUTPUT);
  pinMode(right_lower_motor_dir, OUTPUT);
  pinMode(left_lower_motor_dir, OUTPUT);

  pinMode(13, OUTPUT);
  
  Serial.begin(9600);

  Serial.println("ready");

}

void loop() {
  while (Serial.available() > 0)
  {
    Serial.readBytesUntil('\n', buf, 4);
    motorNumber = atoi(buf);

    //Serial.println(motorNumber);

    delay(10);
  
    Serial.readBytesUntil('\n', buf, 4);
    motorVelocity = atoi(buf);
    motorVelocity -= 355;
    
    if(motorVelocity < 0)
    {
      motorVelocity = abs(motorVelocity);
      motorDirection = 1;
    }

    else
    {
      motorDirection = 0;
    }
        
    switch (motorNumber) {
        case 0:
            analogWrite(right_upper_motor, motorVelocity);
            digitalWrite(right_upper_motor_dir, motorDirection);
            break;
        case 1:
            analogWrite(left_upper_motor, motorVelocity);
            digitalWrite(left_upper_motor_dir, motorDirection);
            break;
        case 2:
            analogWrite(right_lower_motor, motorVelocity);
            digitalWrite(right_lower_motor_dir, motorDirection);
            break;
        case 3:
            analogWrite(left_lower_motor, motorVelocity);
            digitalWrite(left_lower_motor_dir, motorDirection);
            break;
        default:
            // statements
            break;
        }
  }

}