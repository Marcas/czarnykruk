const express = require('express');
const path = require('path');

const fs = require('fs')

const app = express();

const port = 8080;

app.use(express.static(path.join(__dirname, 'public')));

// sendFile will go here
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
  });
  

const httpsOptions = {
    cert: fs.readFileSync(path.join(__dirname,'server.cert')),
    key: fs.readFileSync(path.join(__dirname,'server.key'))
}
const https = require('https').createServer(httpsOptions, app)

https.listen(port, function(){
    console.log('Server started at https://192.168.0.15:' + port);
})

// USART communcation

const SocketOptions = { /* ... */ };

const io = require('socket.io')(https, SocketOptions);

const SerialPort = require('serialport')

const Ready = require('@serialport/parser-ready')
const Readline = require('@serialport/parser-readline')

const Serport = new SerialPort('/dev/ttyACM0')

const parserReady = Serport.pipe(new Ready({ delimiter: 'ready' }))
parserReady.on('ready', setup)

let right_upper_motor = 0
let left_upper_motor = 1
let right_lower_motor = 2
let left_lower_motor = 3

let buff = 0;

function setup()
{
  SetMotor(right_upper_motor, 0);
  SetMotor(left_upper_motor, 0);
  SetMotor(right_lower_motor, 0);
  SetMotor(left_lower_motor, 0);
}

function SetMotor(motor, power)
{ 
  buff = Buffer.from(`00${motor}\n`);

  Serport.write(buff, function(err) {
    if (err) {
      return console.log('Error on write: ', err.message)
    }
  })

  power+=355;

  buff = Buffer.from(`${power}\n`);

  Serport.write(buff, function(err) {
    if (err) {
      return console.log('Error on write: ', err.message)
    }
  })
}

function driveForward()
{
    SetMotor(right_upper_motor, 55)
    SetMotor(left_upper_motor, -55)
    SetMotor(right_lower_motor, 55)
    SetMotor(left_lower_motor, -55)
}

function driveRight()
{
    SetMotor(right_upper_motor, -50)
    SetMotor(left_upper_motor, -50)
    SetMotor(right_lower_motor, 50)
    SetMotor(left_lower_motor, 50)
}

function driveLeft()
{
    SetMotor(right_upper_motor, 50)
    SetMotor(left_upper_motor, 50)
    SetMotor(right_lower_motor, -50)
    SetMotor(left_lower_motor, -50)
}

function driveBack()
{
    SetMotor(right_upper_motor, -50)
    SetMotor(left_upper_motor, 50)
    SetMotor(right_lower_motor, -50)
    SetMotor(left_lower_motor, 50)
}
function rotateRight()
{
    SetMotor(right_upper_motor, -50)
    SetMotor(left_upper_motor, -50)
    SetMotor(right_lower_motor, -50)
    SetMotor(left_lower_motor, -50)
}
function rotateLeft()
{
    SetMotor(right_upper_motor, 50)
    SetMotor(left_upper_motor, 50)
    SetMotor(right_lower_motor, 50)
    SetMotor(left_lower_motor, 50)
}


io.sockets.on('connection', function (socket) {

    console.log('A user connected');
    setup();

    socket.on("button", function(data)
    {   
        let n = data;
        // console.log(n);
        switch (n)
        {
            case "left":
                driveLeft()
                break;
            case "right":
                driveRight()
                break;
            case "up":
                driveForward()
                break;
            case "down":
                driveBack()
                break;
            case "rotleft":
                rotateLeft()
                break;
            case "rotright":
                rotateRight()
                break;
            case "stop":
                setup()
                break;
            default:
                setup()
                console.log('stop');

        }
    });


})

// Reading data
// const parser = Serport.pipe(new Readline({ delimiter: '\r\n' }))
// parser.on('data', console.log)
