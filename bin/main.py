import serial
from time import sleep

ser = serial.Serial ("/dev/ttyACM2", 9600) 
received_data = 1   #Open port with baud rate
ser.flush()
right_upper_motor = 0
left_upper_motor = 1
right_lower_motor = 2
left_lower_motor = 3

def setMotor(motor, power):
    sleep(0.03)
    #power += 10
    ser.write(motor)
    ser.write(b"\n")
    sleep(0.03)
    ser.write(power)
    ser.write(b"\n")
    # ser.write(b"\n")


def driveForward():
    setMotor(right_upper_motor, 255)
    setMotor(left_upper_motor, 255)
    setMotor(right_lower_motor, 255)
    setMotor(right_lower_motor, 255)

def driveRight():
    setMotor(right_upper_motor, -50)
    setMotor(left_upper_motor, 50)
    setMotor(right_lower_motor, 50)
    setMotor(right_lower_motor, -50)

def driveLeft():
    setMotor(right_upper_motor, 50)
    setMotor(left_upper_motor, -50)
    setMotor(right_lower_motor, -50)
    setMotor(right_lower_motor, 50)

def driveBack():
    setMotor(right_upper_motor, -50)
    setMotor(left_upper_motor, -50)
    setMotor(right_lower_motor, -50)
    setMotor(right_lower_motor, -50)
sleep(2)
driveForward()
while ser.inWaiting()>0:
    print(ser.readline().decode('utf-8'))
    sleep(1)