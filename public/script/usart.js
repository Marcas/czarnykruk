const socket = io();

$( document ).ready(function() {
  socket.emit("ok", "oki");
});

let emitOnce = true;

function buttonInf(n)
{

  if(n != "stop")
  {
    if(emitOnce) 
    {
    socket.emit("button", n);

    emitOnce = false;
    }
  }
  else{
    emitOnce = true;
    socket.emit("button", n);
  }
}