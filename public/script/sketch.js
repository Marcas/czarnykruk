let right = [];
let left = [];
let up = [];
let down = [];

function preload() {
  for (let i = 0; i < 100; i++) {
    let index = nf(i + 1, 4, 0);
    right[i] = loadImage(`data/right${index}.png`);
    left[i] = loadImage(`data/left${index}.png`);
    up[i] = loadImage(`data/up${index}.png`);
    down[i] = loadImage(`data/down${index}.png`);
  }
}

let shapeClassifier;

function setup() {
  createCanvas(400, 400);
  //background(0);
  //image(left[0], 0, 0, width, height);

  let options = {
    inputs: [64, 64, 4],
    task: 'imageClassification',
    debug: true
  };
  shapeClassifier = ml5.neuralNetwork(options);

  for (let i = 0; i < right.length; i++) {
    shapeClassifier.addData({ image: right[i] }, { label: 'right' });
    shapeClassifier.addData({ image: left[i] }, { label: 'left' });
    shapeClassifier.addData({ image: up[i] }, { label: 'up' });
    shapeClassifier.addData({ image: down[i] }, { label: 'down' });
  }
  shapeClassifier.normalizeData();
  shapeClassifier.train({ epochs: 50 }, finishedTraining);
}

function finishedTraining() {
  console.log('finished training!');
  shapeClassifier.save();
}