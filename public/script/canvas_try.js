let sketch = function(p) {

    let shapeClassifier;
    let canvas;
    let resultsDiv;
    let inputImage;
    let clearButton;
    let video;

    p.setup = function(){

        canvas = p.createCanvas(400, 400);

        let constraints = {
            video: {
            mandatory: {
                maxWidth: 64,
                maxHeight: 64
            },
            optional: [{ maxFrameRate: 10 }]
            },
            audio: false
        };

        video = p.createCapture(p.VIDEO);
        video.size(64, 64)
        //video.hide();

        
        let options = {
            inputs: [64, 64, 4],
            task: 'imageClassification',
        };
        shapeClassifier = ml5.neuralNetwork(options);
          const modelDetails = {
            model: 'model/model.json',
            metadata: 'model/model_meta.json',
            weights: 'model/model.weights.bin',
          };

        p.background(255);
        resultsDiv = p.createDiv('loading model');
        inputImage = p.createGraphics(64, 64);
        shapeClassifier.load(modelDetails, modelLoaded);
    };

    p.draw = function()
    {
        p.image(video, 0, 0, p.width, p.height);
        p.filter(p.THRESHOLD, 0.4);
    }

    function modelLoaded() {
        console.log('model ready!');
        classifyImage();
      }
      
      function classifyImage() {
        shapeClassifier.classify(
          {
            image: video
          },
          gotResults
        );
      }
      
      function gotResults(err, results) {
        if (err) {
          console.error(err);
          return;
        }
        let label = results[0].label;
        let confidence = p.nf(100 * results[0].confidence, 2, 0);
        resultsDiv.html(`${label} ${confidence}%`);
        classifyImage();
      }
};

new p5(sketch, 'canvasvideo');

